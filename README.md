UDF Manager Module
==================

The `fscm-udf-manager` module configures, builds and loads UDF libraries. It
parses the output of the compiler log and prints only the parsed errors and
warnings to the fluent console. Below the installation and basic usage of this
module is described, if you are looking for the API documentation
[click here](doc/README.md) (located in the `doc` folder).

If you'd like to contribute, please read the 
[contribution guide](CONTRIBUTING.md) first.

Installation
------------
Before you can install the UDF manager module you have to properly setup the
module manager. Follow the 
[Fluent Scheme Module Manager](https://gitlab.com/jkuisw/fscm-module-manager)
guide to set it up.

Now you need to clone the module repository to your computer. Choose a proper
location where you want to clone it, preferably the same directory where the
module manager repository is located (e.g.: `/home/<user name>/jkuisw`). Go into
that directory and clone the repository, as follows:

```shell
cd "/home/<user name>/jkuisw"
git clone --recurse-submodules git@gitlab.com:jkuisw/fscm-udf-manager.git
```

After cloning the repository ensure that the `fscm-udf-manager` module is 
registered to the module manager, as described in the 
[module manager setup](https://gitlab.com/jkuisw/fscm-module-manager#register-modules) 
(`add-module-paths` procedure).

Usage
-----
To use the UDF manager module you have to import it with a proper import name.
With the command `configure-udf-library` you define where are the source (and 
header) files located, choose a library name and a target directory (see the
[API documentation](doc/README.md) for details). When the library is properly
configured you can build and load (if the build was successful) it with the
`build-load-udf-library` procedure. The following example code shows the
configuration, building and loading of a UDF library:

```scheme
; Import the UDF manager module.
(import "udfman" "fscm-udf-manager")

; Define the location of the source files of the library.
(define source-files (list
  "/absolute/path/to/source/file1.c"
  "/absolute/path/to/source/file2.c"
  "/absolute/path/to/source/file3.c"
  "/absolute/path/to/source/file4.c"
))

; Define the location of the header files of the library.
(define header-files (list
  "/absolute/path/to/header/file1.h"
  "/absolute/path/to/header/file2.h"
  "/absolute/path/to/header/file3.h"
  "/absolute/path/to/header/file4.h"
))

; Configure the UDF library.
(udfman/configure-udf-library
  "/path/to/the/target/directory"
  "testlib" ; name of the UDF library
  source-files ; list of source files
  header-files ; list of header files
)

; Build and load the UDF library.
(udfman/build-load-udf-library "testlib")
```

The full documentation for all procedures exported by the UDF manager module can
be found in the [API documentation](doc/README.md).
