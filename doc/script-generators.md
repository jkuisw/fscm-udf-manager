:arrow_left: [Go back to the overview.](/doc/README.md)
  
  
  
  
# File: script-generators.scm
This file implements procedures for generating some scripts, such as:
+ build script
+ parse build log (parsing of errors and warnings)

:arrow_right: [Go to source code of this file.](/src/script-generators.scm)

## Private Procedures
| Name | Description |
|:---- |:----------- |
| [:page_facing_up: generate-build-script](#generate-build-script) | Generates the build script for building the library. The scripts forces theo... |
| [:page_facing_up: generate-build-log-parser](#generate-build-log-parser) | Generates a script to parse the build output from the logging file. It parse... |

## Procedure Documentation

### generate-build-script

#### Syntax
```scheme
(generate-build-script library-directory log-file-name)
```

:arrow_right: [Go to source code of this procedure.](/src/script-generators.scm#L24)

#### Description
Generates the build script for building the library. The scripts forces the
output language of the make command to "en_US.UTF-8" (US english) so that the
output can be parsed independent of the localization.

#### Parameters
##### `library-directory`  
The directory of the library, this is where the global
makefile is located. The build will be executed in this directory.

##### `log-file-name`  
The name of the log file. The output of the build command
will be logged to std out and to the log file. The log file will be
created in the library directory.


#### Returns
The path to the build script.

-------------------------------------------------
### generate-build-log-parser

#### Syntax
```scheme
(generate-build-log-parser dir build-log parse-results)
```

:arrow_right: [Go to source code of this procedure.](/src/script-generators.scm#L96)

#### Description
Generates a script to parse the build output from the logging file. It parses
the log output for warnings and errors and saves the formatted errors and
warnings to the parse results file. The path to the generated script will be
returned

#### Parameters
##### `dir`  
The directory of where the build log file is located..

##### `build-log`  
The name of the build log file.

##### `parse-results`  
The name of the parse results file, which will be created by
the generated script in the given directory (dir parameter).


#### Returns
The path to the generated parse script.

