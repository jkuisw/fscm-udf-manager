:arrow_left: [Go back to the overview.](/doc/README.md)
  
  
  
  
# File: udf-manager.scm
Implements procedures for configuring, building and loading of fluent UDF 
libraries.

:arrow_right: [Go to source code of this file.](/src/udf-manager.scm)

## Exported Procedures
| Name | Description |
|:---- |:----------- |
| [:page_facing_up: configure-udf-library](#configure-udf-library) | Adds a new library configuration to the libraries configuration list. |
| [:page_facing_up: build-udf-library](#build-udf-library) | Builds the library, identified by the given library name. |
| [:page_facing_up: load-udf-library](#load-udf-library) | Load the library, identified by the given library name. |
| [:page_facing_up: build-load-udf-library](#build-load-udf-library) | Builds and loads the library, identified by the given library name. |

## Private Procedures
| Name | Description |
|:---- |:----------- |
| [:page_facing_up: get-config-entry](#get-config-entry) | Retrieves a library configuration entry of the libraries configuration list,... |
| [:page_facing_up: create-directory-structure](#create-directory-structure) | Creates the directory structure for the given library configuration. |
| [:page_facing_up: create-user-udf-file](#create-user-udf-file) | Creates the user.udf file, which contains the lists of source and header fil... |
| [:page_facing_up: create-symbolic-links-to-files](#create-symbolic-links-to-files) | Creates the symbolic links to the source and header files in the librarydire... |

## Procedure Documentation

### get-config-entry

#### Syntax
```scheme
(get-config-entry libname)
```

:arrow_right: [Go to source code of this procedure.](/src/udf-manager.scm#L23)

#### Description
Retrieves a library configuration entry of the libraries configuration list,
identified by the given library name.

#### Parameters
##### `libname`  
The name of the library for which the configuration should be
retrieved.


#### Returns
The configuration of the library if found, false otherwise.

-------------------------------------------------
### create-directory-structure

#### Syntax
```scheme
(create-directory-structure config)
```

:arrow_right: [Go to source code of this procedure.](/src/udf-manager.scm#L64)

#### Description
Creates the directory structure for the given library configuration.

#### Parameters
##### `config`  
The library configuration, for which the directory structure should
be created.


#### Returns
The path to the library directory. This is the directory where the
directory structure was created and where the build should be executed.

-------------------------------------------------
### create-user-udf-file

#### Syntax
```scheme
(create-user-udf-file config)
```

:arrow_right: [Go to source code of this procedure.](/src/udf-manager.scm#L149)

#### Description
Creates the user.udf file, which contains the lists of source and header files
and some other settings. The file will be saved in the library directory under
the source ("src") folder.

#### Parameters
##### `config`  
The library configuration, for which the user.udf file should be
created.



-------------------------------------------------
### create-symbolic-links-to-files

#### Syntax
```scheme
(create-symbolic-links-to-files config)
```

:arrow_right: [Go to source code of this procedure.](/src/udf-manager.scm#L216)

#### Description
Creates the symbolic links to the source and header files in the library
directory under the source ("src") folder.

#### Parameters
##### `config`  
The library configuration.



-------------------------------------------------
### configure-udf-library

#### Syntax
```scheme
(configure-udf-library dir libname source-files [header-files])
```

:arrow_right: [Go to source code of this procedure.](/src/udf-manager.scm#L271)

#### Export Name
`<import-name>/configure-udf-library`

#### Description
Adds a new library configuration to the libraries configuration list.

#### Parameters
##### `dir`  
The directory in which the library should be created, normally this is
the directory where you satred fluent.

##### `libname`  
The name of the library.

##### `source-files`  
A list of source files (*.c files) for the library.

##### `header-files`  
_Attributes: optional, default: `'()`_  
A list of header files (*.h files)
for the library.



-------------------------------------------------
### build-udf-library

#### Syntax
```scheme
(build-udf-library libname)
```

:arrow_right: [Go to source code of this procedure.](/src/udf-manager.scm#L356)

#### Export Name
`<import-name>/build-udf-library`

#### Description
Builds the library, identified by the given library name.

#### Parameters
##### `libname`  
The name of the library to build.


#### Returns
True if the build was successful, false otherwise.
NOTE: If there are warnings but no errors, than true is returned too.

-------------------------------------------------
### load-udf-library

#### Syntax
```scheme
(load-udf-library libname)
```

:arrow_right: [Go to source code of this procedure.](/src/udf-manager.scm#L540)

#### Export Name
`<import-name>/load-udf-library`

#### Description
Load the library, identified by the given library name.

#### Parameters
##### `libname`  
The name of the library to load.


#### Returns
True if successfully loaded the library, false otherwise.

-------------------------------------------------
### build-load-udf-library

#### Syntax
```scheme
(build-load-udf-library libname)
```

:arrow_right: [Go to source code of this procedure.](/src/udf-manager.scm#L589)

#### Export Name
`<import-name>/build-load-udf-library`

#### Description
Builds and loads the library, identified by the given library name.

#### Parameters
##### `libname`  
The name of the library to build and load.


#### Returns
True if successfully builded and loaded, false otherwise.

