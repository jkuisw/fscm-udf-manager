# Module: fscm-udf-manager
This is the overview of the module documentation. In this file you can find a list with all source files of the module, a list of all exported procedures of the module and a list of all module private procedures. The name of each list entry links to the specific documentation of the source file, exported procedure or private procedure.  

## Source Files
A list of all source files of this module.  

| Name | Description |
|:---- |:----------- |
| [:page_facing_up: script-generators](/doc/script-generators.md) | This file implements procedures for generating some scripts, such as:\n+ bui... |
| [:page_facing_up: udf-manager](/doc/udf-manager.md) | Implements procedures for configuring, building and loading of fluent UDF \n... |

## Exported Procedures
A list of all exported procedures of this module. The procedure names are the internal procedure names. Normally this is the same as the exported name, prepended with the import name, but thy can differ. Look into the detailed procedure documentation to get the export name of the procedure. Exported procedures are imported into a source file with the `import` procedure (see the fscm-module-manager module for further details), which takes the import name as the first parameter. Therefore every exported procedure of a module will be imported according to the following syntax:  
`<import name>/<export name of the procedure>`.  

| Name | Description |
|:---- |:----------- |
| [:page_facing_up: configure-udf-library](/doc/udf-manager.md#configure-udf-library) | Adds a new library configuration to the libraries configuration list. |
| [:page_facing_up: build-udf-library](/doc/udf-manager.md#build-udf-library) | Builds the library, identified by the given library name. |
| [:page_facing_up: load-udf-library](/doc/udf-manager.md#load-udf-library) | Load the library, identified by the given library name. |
| [:page_facing_up: build-load-udf-library](/doc/udf-manager.md#build-load-udf-library) | Builds and loads the library, identified by the given library name. |

## Private Procedures
A list of all private procedures of this module. This procedures are not accessible from outside the module.  

| Name | Description |
|:---- |:----------- |
| [:page_facing_up: generate-build-script](/doc/script-generators.md#generate-build-script) | Generates the build script for building the library. The scripts forces theo... |
| [:page_facing_up: generate-build-log-parser](/doc/script-generators.md#generate-build-log-parser) | Generates a script to parse the build output from the logging file. It parse... |
| [:page_facing_up: get-config-entry](/doc/udf-manager.md#get-config-entry) | Retrieves a library configuration entry of the libraries configuration list,... |
| [:page_facing_up: create-directory-structure](/doc/udf-manager.md#create-directory-structure) | Creates the directory structure for the given library configuration. |
| [:page_facing_up: create-user-udf-file](/doc/udf-manager.md#create-user-udf-file) | Creates the user.udf file, which contains the lists of source and header fil... |
| [:page_facing_up: create-symbolic-links-to-files](/doc/udf-manager.md#create-symbolic-links-to-files) | Creates the symbolic links to the source and header files in the librarydire... |

