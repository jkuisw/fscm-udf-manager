;*******************************************************************************
; Implements procedures for configuring, building and loading of fluent UDF 
; libraries.

;*******************************************************************************
; Dependencies
(import "utils" "fscm-utils" "1.*.*")
(import "fs" "fscm-file-system-ops" "1.*.*")

;*******************************************************************************
; Module global variables.
(define libraries-config-list '())

;*******************************************************************************
; Retrieves a library configuration entry of the libraries configuration list,
; identified by the given library name.
;
; Parameters:
;   libname - The name of the library for which the configuration should be
;     retrieved.
;
; Returns: The configuration of the library if found, false otherwise.
(define (get-config-entry libname) (let (
    (curr-cfg-entry '())
    (config-libname "")
    (key "")
  )
  ; Loop through the libraries configuration list to search for the
  ; configuration entry of the library.
  (let next-cfg-entry ((remaining-entries libraries-config-list))
    (if (null? remaining-entries) (begin
      ; Reached end of list, therefore no configuration exists for the library.
      ; Return false.
      #f
    ) (begin ; else
      ; Get current entry.
      (set! curr-cfg-entry (car remaining-entries))

      ; Check if the current configuration entry is the one for the library.
      (set! key 'library-name)
      (set! config-libname (utils/map-get-value curr-cfg-entry key))
      (if (eqv? config-libname 'key-not-found) (error "Key not found!" key))
      (if (string=? config-libname libname) (begin
         ; Found configuration entry for the library, therefore return it.
         curr-cfg-entry
      ) (begin ; else
        ; Current configuration entry is not for the library, therefore check
        ; the next one.
        (next-cfg-entry (cdr remaining-entries))
      ))
    ))
  )
))

;*******************************************************************************
; Creates the directory structure for the given library configuration.
;
; Parameters:
;   config - The library configuration, for which the directory structure should
;     be created.
;
; Returns: The path to the library directory. This is the directory where the
;   directory structure was created and where the build should be executed.
(define (create-directory-structure config) (let (
    (libname '())
    (solver-version '())
    (root-dir '())
    (lib-dir "")
    (src-dir "")
    (arch-dir "")
    (solver-version-dir "")
    (solver-version-host-dir "")
    (solver-version-node-dir "")
    (key "")
  )
  ; Get values from config.
  (set! key 'library-name)
  (set! libname (utils/map-get-value config key))
  (if (eqv? libname 'key-not-found) (error "Key not found!" key))

  (set! key 'solver-version)
  (set! solver-version (utils/map-get-value config key))
  (if (eqv? solver-version 'key-not-found) (error "Key not found!" key))

  (set! key 'library-root-dir)
  (set! root-dir (utils/map-get-value config key))
  (if (eqv? root-dir 'key-not-found) (error "Key not found!" key))

  ; Initialize general directory variables.
  (set! lib-dir (format #f "~a/~a" root-dir libname))
  (set! src-dir (format #f "~a/src" lib-dir))
  (set! arch-dir (format #f "~a/~a" lib-dir (fluent-arch)))

  ; Initialize directory variables depending on solver version.
  (cond
    ((string=? solver-version "2d") (begin
      (set! solver-version-dir (format #f "~a/2d" arch-dir))
      (set! solver-version-host-dir (format #f "~a/2d_host" arch-dir))
      (set! solver-version-node-dir (format #f "~a/2d_node" arch-dir))
    ))
    ((string=? solver-version "2ddp") (begin
      (set! solver-version-dir (format #f "~a/2ddp" arch-dir))
      (set! solver-version-host-dir (format #f "~a/2ddp_host" arch-dir))
      (set! solver-version-node-dir (format #f "~a/2ddp_node" arch-dir))
    ))
    ((string=? solver-version "3d") (begin
      (set! solver-version-dir (format #f "~a/3d" arch-dir))
      (set! solver-version-host-dir (format #f "~a/3d_host" arch-dir))
      (set! solver-version-node-dir (format #f "~a/3d_node" arch-dir))
    ))
    ((string=? solver-version "3ddp") (begin
      (set! solver-version-dir (format #f "~a/3ddp" arch-dir))
      (set! solver-version-host-dir (format #f "~a/3ddp_host" arch-dir))
      (set! solver-version-node-dir (format #f "~a/3ddp_node" arch-dir))
    ))
    (else (error (format "~a ~a"
      "Unsupported solver version, should be one of: \"2d\", \"2ddp\", \"3d\""
      ", \"3ddp\"!" solver-version
    )))
  )

  ; Create the directories if not existing.
  (if (not (file-directory? lib-dir)) (fs/make-directory lib-dir))
  (if (not (file-directory? src-dir)) (fs/make-directory src-dir))
  (if (not (file-directory? arch-dir)) (fs/make-directory arch-dir))

  (if (not (file-directory? solver-version-dir))
    (fs/make-directory solver-version-dir)
  )
  (if (not (file-directory? solver-version-host-dir))
    (fs/make-directory solver-version-host-dir)
  )
  (if (not (file-directory? solver-version-node-dir))
    (fs/make-directory solver-version-node-dir)
  )

  ; Return the library directory.
  lib-dir
))

;*******************************************************************************
; Creates the user.udf file, which contains the lists of source and header files
; and some other settings. The file will be saved in the library directory under
; the source ("src") folder.
;
; Parameters:
;   config - The library configuration, for which the user.udf file should be
;     created.
(define (create-user-udf-file config) (letrec (
    (libname '())
    (root-dir '())
    (source-files '())
    (header-files '())
    (user-udf-path "")
    (file-port 0)
    (key "")
    (build-files-list-string (lambda (files) (let next-file ((remaining-files files))
      (if (null? remaining-files) (begin
        ; Reached end of list, therefore return an empty string.
        ""
      ) (begin ; else
        ; Append filename and go to the next file.
        (string-append
          (strip-directory (car remaining-files)) " "
          (next-file (cdr remaining-files))
        )
      ))
    )))
  )
  ; Get values from config.
  (set! key 'library-name)
  (set! libname (utils/map-get-value config key))
  (if (eqv? libname 'key-not-found) (error "Key not found!" key))

  (set! key 'library-root-dir)
  (set! root-dir (utils/map-get-value config key))
  (if (eqv? root-dir 'key-not-found) (error "Key not found!" key))

  (set! key 'source-files)
  (set! source-files (utils/map-get-value config key))
  (if (eqv? source-files 'key-not-found) (error "Key not found!" key))

  (set! key 'header-files)
  (set! header-files (utils/map-get-value config key))
  (if (eqv? header-files 'key-not-found) (error "Key not found!" key))

  ; Build the path to the "user.udf" file (where it should be saved) and open it.
  (set! user-udf-path (format #f "~a/~a/src/user.udf" root-dir libname))
  (set! file-port (open-output-file user-udf-path))

  ; Generate the "user.udf" file.
  ; Write source files list.
  (display "CSOURCES = " file-port)
  (display (build-files-list-string source-files) file-port)
  (display "\n" file-port)

  ; Write header files list.
  (display "HSOURCES = " file-port)
  (display (build-files-list-string header-files) file-port)
  (display "\n" file-port)

  ; Write other settings.
  (display (format #f "FLUENT_INC = ~a\n" (utils/get-fluent-install-dir)) file-port)
  (display "GPU_SUPPORT=off\n" file-port)

  ; Close the file port.
  (close-output-port file-port)
))

;*******************************************************************************
; Creates the symbolic links to the source and header files in the library
; directory under the source ("src") folder.
;
; Parameters:
;   config - The library configuration.
(define (create-symbolic-links-to-files config) (letrec (
    (key "")
    (libname '())
    (root-dir '())
    (source-files '())
    (header-files '())
    (link-target "")
    (create-link-to-file (lambda (file-path)
      ; Only create the symbolic link if it does not already exist or if it is
      ; corrupted.
      (if (not (file-exists? (string-append
          link-target
          (strip-directory file-path)
        )))
        (fs/create-symbolic-link file-path link-target)
      )
    ))
  )
  ; Get values from config.
  (set! key 'library-name)
  (set! libname (utils/map-get-value config key))
  (if (eqv? libname 'key-not-found) (error "Key not found!" key))

  (set! key 'library-root-dir)
  (set! root-dir (utils/map-get-value config key))
  (if (eqv? root-dir 'key-not-found) (error "Key not found!" key))

  (set! key 'source-files)
  (set! source-files (utils/map-get-value config key))
  (if (eqv? source-files 'key-not-found) (error "Key not found!" key))

  (set! key 'header-files)
  (set! header-files (utils/map-get-value config key))
  (if (eqv? header-files 'key-not-found) (error "Key not found!" key))

  ; Build link target path.
  (set! link-target (format #f "~a/~a/src/" root-dir libname))

  ; Create links to source files.
  (for-each create-link-to-file source-files)

  ; Create links to header files.
  (for-each create-link-to-file header-files)
))

;*******************************************************************************
; Adds a new library configuration to the libraries configuration list.
;
; Parameters:
;   dir - The directory in which the library should be created, normally this is
;     the directory where you satred fluent.
;   libname - The name of the library.
;   source-files - A list of source files (*.c files) for the library.
;   header-files - [optional, default: '()] A list of header files (*.h files)
;     for the library.
(define (configure-udf-library dir libname source-files . args) (let (
    (key "")
    (config-libname "")
    (header-files '())
    (curr-cfg-entry '())
    (new-config-entry '())
    (solver-version (utils/get-fluent-solver-version))
  )
  ; Check directory parameter.
  (if (not (or (symbol? dir) (string? dir))) (error "Invalid parameter!" dir))
  (if (not (file-directory? dir)) (error "Directory does not exist!" dir))

  ; Check source-files parameter.
  (if (not (pair? source-files)) (error "Invalid parameter!" source-files))
  (for-each (lambda (src-file) (if (not (file-exists? src-file))
    (error "Source file does not exist!" src-file))) source-files)

  ; Check library name parameter.
  (if (not (or (symbol? libname) (string? libname)))
    (error "Invalid parameter!" libname)
  )

  ; Parse optional arguments.
  (if (> (length args) 0) (begin
    ; Header files list parameter passed.
    (set! header-files (list-ref args 0))
    ; Check if parameter is valid.
    (if (not (pair? header-files)) (error "Invalid parameter!" header-files))
    (for-each (lambda (h-file) (if (not (file-exists? h-file))
      (error "Header file does not exist!" h-file))) header-files)
  ))

  ; Build the new configuration entry.
  (set! new-config-entry (list
    (list 'library-name libname)
    (list 'library-root-dir dir)
    (list 'source-files source-files)
    (list 'header-files header-files)
    (list 'solver-version solver-version)
    (list 'successfully-build #f)
  ))

  ; Loop through the libraries configuration list to search for the
  ; configuration entry of the library. If not found, add a new entry.
  (let next-cfg-entry ((remaining-entries libraries-config-list))
    (if (null? remaining-entries) (begin
      ; Reached end of list, therefore no configuration exists for the library
      ; yet.
      (if (<= (length libraries-config-list) 0)
        (set! libraries-config-list (list new-config-entry))
        (utils/append-to-list libraries-config-list new-config-entry)
      )
    ) (begin ; else
      ; Get current entry.
      (set! curr-cfg-entry (car remaining-entries))

      ; Check if the current configuration entry is the one for the library.
      (set! key 'library-name)
      (set! config-libname (utils/map-get-value curr-cfg-entry key))
      (if (eqv? config-libname 'key-not-found) (error "Key not found!" key))
      (if (string=? config-libname libname) (begin
         ; Found configuration entry for the library, therefore update the
         ; settings.
         (utils/map-set-value curr-cfg-entry 'library-root-dir dir)
         (utils/map-set-value curr-cfg-entry 'source-files source-files)
         (utils/map-set-value curr-cfg-entry 'header-files header-files)
         (utils/map-set-value curr-cfg-entry 'solver-version solver-version)
         (utils/map-set-value curr-cfg-entry 'successfully-build #f)
      ) (begin ; else
        ; Current configuration entry is not for the library, therefore check
        ; the next one.
        (next-cfg-entry (cdr remaining-entries))
      ))
    ))
  )
))

;*******************************************************************************
; Builds the library, identified by the given library name.
;
; Parameters:
;   libname - The name of the library to build.
;
; Returns: True if the build was successful, false otherwise.
;   NOTE: If there are warnings but no errors, than true is returned too.
(define (build-udf-library libname) (letrec (
    (fluent-release-dir (string-append (utils/get-fluent-install-dir) "/fluent"
      (utils/get-fluent-version-string)))
    (fluent-udf-include-path "src/udf")
    (makefile-udf2-name "makefile.udf2")
    (makefile-udf-name "makefile.udf")
    (libconfig '())
    (lib-dir "")
    (src-dir "")
    (build-log-file "build.log")
    (build-script-path "")
    (parse-script-path "")
    (build-results-file "build-results.log")
    (file-port 0)
    (text "")
    (found-errors #f)
    (found-warnings #f)
    (copy-makefile (lambda (target-dir makefile-name) (let (
        (makefile-source-path (string-append
          fluent-release-dir "/" fluent-udf-include-path "/" makefile-name
        ))
        (makefile-target-path (string-append target-dir "/" makefile-name))
      )
      (if (not (file-exists? (basename makefile-target-path))) (begin
        (fs/copy-file makefile-source-path (string-append target-dir "/"))
        (fs/rename-file-or-dir makefile-target-path (basename makefile-target-path))
      ))
    )))
  )
  ; Check if parameter is valid.
  (if (not (or (string? libname) (symbol? libname)))
    (error "Invalid parameter!" libname)
  )

  ; Get configuration of library.
  (set! libconfig (get-config-entry libname))
  (if (eqv? libconfig #f) (error "No configuration found for library!" libname))

  ; Build the directory structure.
  ;(display libconfig)
  (set! lib-dir (create-directory-structure libconfig))
  (set! src-dir (string-append lib-dir "/src"))

  ; Copy the makefiles if not already.
  (copy-makefile lib-dir makefile-udf2-name)
  (copy-makefile src-dir makefile-udf-name)

  ; Generate "user.udf" file.
  (create-user-udf-file libconfig)

  ; Create symbolic links to the source and header files.
  (create-symbolic-links-to-files libconfig)

  ; Generate the build script and build the library.
  (set! build-script-path (generate-build-script lib-dir build-log-file))
  (utils/exec-on-bash build-script-path)

  ; Generate the build log output parsing script and execute it.
  (set! parse-script-path (generate-build-log-parser
    lib-dir build-log-file build-results-file))
  (utils/exec-on-bash parse-script-path)

  ; Print the build results by parsing the build results file, generated by the
  ; build log output parsing script.
  (display "Build results:\n")
  (set! file-port (open-input-file (string-append lib-dir "/"
    build-results-file)))
  (let next-line (
      (line (fs/read-line file-port))
      (empty-line-count 0)
      (state 'architecture)
      (objects "")
    )
    (cond
      ; Check if reached end of file.
      ((eof-object? line) (close-input-port file-port))
      ; If current line is empty, count it and read the next line.
      ((= (string-length line) 0)
        (next-line (fs/read-line file-port) (+ empty-line-count 1) state objects)
      )
      ; Check if state is 'architecture: Begin of new target header, read target
      ; architecture.
      ((eqv? state 'architecture)
        (next-line (fs/read-line file-port) 0 'solver-version line)
      )
      ; Check if state is 'solver-version: Second line of new target header,
      ; target solver version.
      ((eqv? state 'solver-version) (begin
        (display (string-append "-> Target " objects "/" line "\n"))
        ; Got to warnings state.
        (next-line (fs/read-line file-port) 0 'warnings '())
      ))
      ; Check if state is 'warnings: Parse warnings of current target.
      ((eqv? state 'warnings)
        (cond
          ((= empty-line-count 0) (begin ; more warnings for current target
            (if (null? objects)
              (set! objects (list line))
              (utils/append-to-list objects line)
            )
            (next-line (fs/read-line file-port) 0 state objects)
          ))
          ((>= empty-line-count 1) (begin ; no warnings or end of warnings
            (if (> (length objects) 0) (begin
              (set! found-warnings #t)

              ; print warnings
              (set! text (if (= (length objects) 1) "warning" "warnings"))

              (display (string-append
                "  " (utils/to-string (length objects)) " " text ":\n"
              ))

              (for-each (lambda (warning)
                (display (string-append "    " warning "\n"))
              ) objects)
            ))
            ; Got to errors state.
            (next-line line empty-line-count 'errors '())
          ))
        )
      )
      ; Check if state is 'errors: Parse errors of current target.
      ((eqv? state 'errors)
        (if (<= empty-line-count 1) (begin ; more errors for current target
          (if (null? objects)
            (set! objects (list line))
            (utils/append-to-list objects line)
          )
          (next-line (fs/read-line file-port) 0 state objects)
        ) (begin ; no errors or end of errors
          (if (> (length objects) 0) (begin
            (set! found-errors #t)

            ; print errors
            (set! text (if (= (length objects) 1) "error" "errors"))

            (display (string-append
              "  " (utils/to-string (length objects)) " " text ":\n"
            ))

            (for-each (lambda (warning)
              (display (string-append "    " warning "\n"))
            ) objects)
          ))
          ; Finished target, go to next one.
          (next-line line 0 'architecture '())
        ))
      )
    )
  )

  ; Delete build results file.
  (remove-file (string-append lib-dir "/" build-results-file))

  ; Print general infos.
  (if (or found-warnings found-errors) (begin
    (utils/map-set-value libconfig 'successfully-build #f)
    (display (string-append "For further information see the build log: "
      lib-dir "/" build-log-file "\n"))
  ))

  (if (and (not found-warnings) (not found-errors)) (begin
    (display "Successful build, no errors or warnings found.\n")
    (utils/map-set-value libconfig 'successfully-build #t)
    #t ; Return true for successful build.
  ) (begin ; else
    (if (and (not found-errors) found-warnings) (begin
      (utils/map-set-value libconfig 'successfully-build #t)
      #t ; Only warnings found, but no errors, therefore return true.
    ) (begin ; else
      (display "Build failed!\n")
      #f ; There are errors, therefore return false.
    ))
  ))
))

;*******************************************************************************
; Load the library, identified by the given library name.
;
; Parameters:
;   libname - The name of the library to load.
;
; Returns: True if successfully loaded the library, false otherwise.
(define (load-udf-library libname) (let (
    (libconfig '())
    (successfully-build #f)
    (root-dir '())
    (successfully-loaded #f)
    (key "")
  )
  ; Check if parameter is valid.
  (if (not (or (string? libname) (symbol? libname)))
    (error "Invalid parameter!" libname)
  )

  ; Get configuration of library.
  (set! libconfig (get-config-entry libname))
  (if (eqv? libconfig #f) (error "No configuration found for library!" libname))

  ; Read configuration values.
  (set! key 'successfully-build)
  (set! successfully-build (utils/map-get-value libconfig key))
  (if (eqv? successfully-build 'key-not-found) (error "Key not found!" key))

  (set! key 'library-root-dir)
  (set! root-dir (utils/map-get-value libconfig key))
  (if (eqv? root-dir 'key-not-found) (error "Key not found!" key))

  ; Check if library was successfully build.
  (if (not successfully-build) (begin
    ; Library not or not successfully build.
    (display (string-append
      "There exists no successful build of the library " libname))
  ) (begin ; else
    ; There exists a successful build of the library, therefore load it.
    (set! successfully-loaded (ti-menu-load-string (string-append
      "/define/user-defined/compiled-functions load \"" root-dir "/" libname "\""
    )))
    (newline)
  ))

  ; Return if successfully loaded the library.
  successfully-loaded
))

;*******************************************************************************
; Builds and loads the library, identified by the given library name.
;
; Parameters:
;   libname - The name of the library to build and load.
;
; Returns: True if successfully builded and loaded, false otherwise.
(define (build-load-udf-library libname) (let (
    (build-success #f)
    (load-success #f)
  )
  ; Check if parameter is valid.
  (if (not (or (string? libname) (symbol? libname)))
    (error "Invalid parameter!" libname)
  )

  ; Build the library.
  (set! build-success (build-udf-library libname))

  ; Load the library if build was successful.
  (if build-success
    (set! load-success (load-udf-library libname))
  )

  ; Return if operation was successful.
  (and build-success load-success)
))

;*******************************************************************************
; Export public procedures.
(export (list
  (list 'configure-udf-library configure-udf-library)
  (list 'build-udf-library build-udf-library)
  (list 'load-udf-library load-udf-library)
  (list 'build-load-udf-library build-load-udf-library)
))

;*******************************************************************************
; TODO
;
; Parameters:
;   par - description
;
; Returns: TODO
