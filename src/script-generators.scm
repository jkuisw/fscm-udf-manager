;*******************************************************************************
; This file implements procedures for generating some scripts, such as:
; + build script
; + parse build log (parsing of errors and warnings)

;*******************************************************************************
; Dependencies
(import "utils" "fscm-utils" "1.*.*")
(import "fs" "fscm-file-system-ops" "1.*.*")

;*******************************************************************************
; Generates the build script for building the library. The scripts forces the
; output language of the make command to "en_US.UTF-8" (US english) so that the
; output can be parsed independent of the localization.
;
; Parameters:
;   library-directory - The directory of the library, this is where the global
;     makefile is located. The build will be executed in this directory.
;   log-file-name - The name of the log file. The output of the build command
;     will be logged to std out and to the log file. The log file will be
;     created in the library directory.
;
; Returns: The path to the build script.
(define (generate-build-script library-directory log-file-name) (let (
    (current-directory "")
    (build-script-name "build-library")
    (build-script-path "")
    (file-port 0)
  )
  ; Retrieve the current directory.
  (set! current-directory (fs/get-current-directory))
  (if (not (eqv? current-directory #f)) (begin
    (set! build-script-path (string-append library-directory "/"
      build-script-name))

    (set! file-port (open-output-file build-script-path))

    ; Change to library directory.
    (display "# Change to the library directory.\n" file-port)
    (display (format #f "cd \"~a\" || exit 1\n\n" library-directory) file-port)

    ; Set the language variable for the make command.
    (display "# Check if LANG environment variable is set.\n" file-port)
    (display "if [ -z ${LANG+x} ]; then\n" file-port)
    (display "  # LANG is unset, therefore set it.\n" file-port)
    (display "  LANG=en_US.UTF-8\n" file-port)
    (display "else\n" file-port)
    (display "  # LANG is set, therefore save a backup.\n" file-port)
    (display "  BACKUP_LANG=$LANG\n" file-port)
    (display "  LANG=en_US.UTF-8\n" file-port)
    (display "fi\n\n" file-port)

    ; Build the library and write it`s output to the log file and std-out.
    (display "# Build the library.\n" file-port)
    (display (format #f "make FLUENT_ARCH=\"~a\" | tee \"~a\"\n\n"
      (fluent-arch) (string-append library-directory "/" log-file-name)
    ) file-port)

    ; Restore the original language setting.
    (display "# Check if BACKUP_LANG environment variable is set.\n" file-port)
    (display "if [ -z ${BACKUP_LANG+x} ]; then\n" file-port)
    (display "  # BACKUP_LANG is unset, therefore unset LANG.\n" file-port)
    (display "  unset LANG\n" file-port)
    (display "else\n" file-port)
    (display "  # BACKUP_LANG is set, therefore restore the value.\n" file-port)
    (display "  LANG=$BACKUP_LANG\n" file-port)
    (display "  unset BACKUP_LANG\n" file-port)
    (display "fi\n\n" file-port)

    ; Go back to the former directory.
    (display "# Go back to the former directory.\n" file-port)
    (display (format #f "cd \"~a\" || exit 1\n\n" current-directory) file-port)

    (close-output-port file-port)
  ) (begin ; else
    (error "Failed to retrieve current directory!")
  ))

  ; Return the path to the build script.
  build-script-path
))

;*******************************************************************************
; Generates a script to parse the build output from the logging file. It parses
; the log output for warnings and errors and saves the formatted errors and
; warnings to the parse results file. The path to the generated script will be
; returned
;
; Parameters:
;   dir - The directory of where the build log file is located..
;   build-log - The name of the build log file.
;   parse-results - The name of the parse results file, which will be created by
;     the generated script in the given directory (dir parameter).
;
; Returns: The path to the generated parse script.
(define (generate-build-log-parser dir build-log parse-results) (let (
    (current-directory "")
    (parse-script-path (string-append dir "/parse-build-log"))
    (results-file-path (string-append dir "/" parse-results))
    (file-port 0)
  )
  ; Retrieve the current directory.
  (set! current-directory (fs/get-current-directory))
  (if (not (eqv? current-directory #f)) (begin
    ; Create the script file.
    (set! file-port (open-output-file parse-script-path))

    (display (string-append
      "#!/bin/bash\n"
      "\n"
      "# settings\n"
      "ARCH=\"\"\n"
      "SOLVER_VERSION=\"\"\n"
      "BASE_FILE_NAME=\"" (basename parse-results) "\"\n"
      (if (> (string-length parse-results) 0)
        (string-append "FILE_EXT=\"." (suffix parse-results) "\"\n")
        "FILE_EXT=\"\"\n"
      )
      "FILE_NAME=\"$BASE_FILE_NAME$FILE_EXT\"\n"
      "\n"
      "# regular expressions\n"
      "ERROR_REGEX=\"([0-9a-zA-Z._-]+):([0-9]+):(|([0-9]+):)\\\\s+error:\\\\s+(.+)\"\n"
      "WARNING_REGEX=\"([0-9a-zA-Z._-]+):([0-9]+):(|([0-9]+):)\\\\s+warning:\\\\s+(.+)\"\n"
      "TARGET_REGEX=\"#\\\\s+building\\\\s+library\\\\s+in\\\\s+([^\\\\/]+)\\\\/(.*)\"\n"
      "\n"
      "# Change to the build log directory and delete result file if existing.\n"
      "cd \"" dir "\" || exit 1\n"
      "rm -f \"$FILE_NAME\"\n"
      "\n"
      "# parse warnings\n"
      "while read line; do\n"
      "  if [[ $line =~ $TARGET_REGEX ]]; then\n"
      "    # Found new target, therefore print to new target file.\n"
      "    ARCH=${BASH_REMATCH[1]}\n"
      "    SOLVER_VERSION=${BASH_REMATCH[2]}\n"
      "    FILE_NAME=\"$BASE_FILE_NAME$ARCH$SOLVER_VERSION$FILE_EXT\"\n"
      "\n"
      "    # Delete target file if already existing.\n"
      "    rm -f $FILE_NAME\n"
      "\n"
      "    # Print architecture and solver version of target.\n"
      "    echo \"$ARCH\" >> $FILE_NAME\n"
      "    echo \"$SOLVER_VERSION\" >> $FILE_NAME\n"
      "  elif [[ $line =~ $WARNING_REGEX ]]; then\n"
      "    # Found a warning.\n"
      "    if [[ -z ${BASH_REMATCH[3]} ]]; then\n"
      "      echo \"${BASH_REMATCH[1]} (line ${BASH_REMATCH[2]})"
      ": ${BASH_REMATCH[5]}\" >> $FILE_NAME\n"
      "    else\n"
      "      echo \"${BASH_REMATCH[1]} (line ${BASH_REMATCH[2]}, col "
      "${BASH_REMATCH[4]}): ${BASH_REMATCH[5]}\" >> $FILE_NAME\n"
      "    fi\n"
      "  fi\n"
      "done < \"" build-log "\"\n"
      "\n"
      "# parse errors\n"
      "while read line; do\n"
      "  if [[ $line =~ $TARGET_REGEX ]]; then\n"
      "    # Found new target, therefore print to according target file.\n"
      "    ARCH=${BASH_REMATCH[1]}\n"
      "    SOLVER_VERSION=${BASH_REMATCH[2]}\n"
      "    FILE_NAME=\"$BASE_FILE_NAME$ARCH$SOLVER_VERSION$FILE_EXT\"\n"
      "\n"
      "    # Print an empty line to the target file to distinguish between\n"
      "    # errors and warnings.\n"
      "    echo \"\" >> $FILE_NAME\n"
      "  elif [[ $line =~ $ERROR_REGEX ]]; then\n"
      "    # Found an error.\n"
      "    if [[ -z ${BASH_REMATCH[3]} ]]; then\n"
      "      echo \"${BASH_REMATCH[1]} (line ${BASH_REMATCH[2]})"
      ": ${BASH_REMATCH[5]}\" >> $FILE_NAME\n"
      "    else\n"
      "      echo \"${BASH_REMATCH[1]} (line ${BASH_REMATCH[2]}, col "
      "${BASH_REMATCH[4]}): ${BASH_REMATCH[5]}\" >> $FILE_NAME\n"
      "    fi\n"
      "  fi\n"
      "done < \"" build-log "\"\n"
      "\n"
      "# Combine all target build results file to one build results file.\n"
      "FILE_NAME=\"$BASE_FILE_NAME$FILE_EXT\"\n"
      "for file in $BASE_FILE_NAME*$FILE_EXT; do\n"
      "  cat $file >> $FILE_NAME\n"
      "\n"
      "  # Print two empty line to seperate targets.\n"
      "  echo "" >> $FILE_NAME\n"
      "  echo "" >> $FILE_NAME\n"
      "\n"
      "  # Remove the target file.\n"
      "  rm -f $file\n"
      "done\n"
      "\n"
      "# Replace some characters in the build results file, which cannot be\n"
      "# displayed by fluent.\n"
      "sed -i -e 's/'$'\\u2018''/`/g' $FILE_NAME\n"
      "sed -i -e 's/'$'\\u2019''/`/g' $FILE_NAME\n"
      "sed -i -e 's/'$'\\u00E2''/`/g' $FILE_NAME\n"
      "\n"
      "# Go back to the former directory.\n"
      "cd \"" current-directory "\" || exit 1\n"
    ) file-port)

    (close-output-port file-port)
  ) (begin ; else
    (error "Failed to retrieve current directory!")
  ))

  ; Return the path to the parse script.
  parse-script-path
))

;*******************************************************************************
; TODO
;
; Parameters:
;   par - description
;
; Returns: TODO
